package main

import (
	"net/http"
	"strconv"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"moolekkari.net/joice/zorodha/routes"
	"moolekkari.net/joice/zorodha/utils"
)

const (
	appPort = "APP_PORT"
)

func main() {
	ginRouter := gin.Default()
	v := ginRouter.Group("v1")
	routes.APIRoutes(v)
	ginRouter.Use(static.Serve("/", static.LocalFile("./public", true)))
	// routes.StaticRoutes(v)

	port, _ := utils.GetEnvInt(appPort, 8000)
	log.WithFields(log.Fields{"method": "main"}).Infoln("server running")

	server := &http.Server{
		Addr:    ":" + strconv.Itoa(port),
		Handler: ginRouter,
	}

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.WithFields(log.Fields{"error": err.Error()}).Fatalf("server listen failed")
		panic(err)
	}

}
