package models

// NSEResponse is a wrapper for reading response from api
type NSEResponse struct {
	Success bool   `json:"success"`
	Results int    `json:"results"`
	Rows    []Rows `json:"rows"`
}

// Rows holds individual nse api response
type Rows struct {
	Symbol           string `json:"Symbol"`
	CompanyName      string `json:"CompanyName"`
	ISIN             string `json:"ISIN"`
	Ind              string `json:"Ind"`
	Purpose          string `json:"Purpose"`
	BoardMeetingDate string `json:"BoardMeetingDate"`
	DisplayDate      string `json:"DisplayDate"`
	SeqID            string `json:"seqId"`
	Details          string `json:"Details"`
}

// Status is an API wrapper
type Status struct {
	StatusCode        int    `json:"status_code"`
	StatusDescription string `json:"description_code"`
	Description       string `json:"description"`
}
