
document.addEventListener("DOMContentLoaded", function(){
    var table = document.getElementById("table")
    table.style.display="none"
    const resource_url = "../v1/api";
    document.getElementById('searchBtn').addEventListener('click', searchFn, false);

    function searchFn() {
        var search = document.getElementById('search').value;
        var period = document.getElementById('period').value;
        
        console.log('Looking for ' + search);
        console.log('Search button works');
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () { // this is what happens when an answer is returned 
            if(xhttp.readyState === 4 && xhttp.status === 200) { // a valid answer
            console.log(xhttp.responseText);
            document.getElementById('table-rows').innerHTML = "";
            populateDataToTable(JSON.parse(xhttp.responseText))
            
            }
        };
        xhttp.open("GET", resource_url+"?symbol=" + search+"&period="+period, false);
        xhttp.send(); // sending the term variable to the search page
    }
})

function populateDataToTable(jsonObject) {
    var tbody = document.getElementById("table-rows");
    console.log(jsonObject.Result.results)
    console.log(typeof(jsonObject.Result.results))
    if (jsonObject.Result.results > 0) {
        table.style.display="block"

        for (obj in jsonObject.Result.rows){
            row = "<tr><td>"+jsonObject.Result.rows[obj].BoardMeetingDate+"</td><td>"+jsonObject.Result.rows[obj].Purpose+"</td><td>"+jsonObject.Result.rows[obj].Details+"</td></tr>"
            tbody.innerHTML+=row;
          }
    } else {
        table.style.display="none"
        window.alert("no results");
    }
    
}