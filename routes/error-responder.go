package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	models "moolekkari.net/joice/zorodha/model"
)

type errorResponse struct {
	Status models.Status `json:"status"`
}

func errorResponder(err error, method string, description string, httpCode int, ctx *gin.Context) {
	if err != nil {
		logrus.WithFields(logrus.Fields{"method": method, "description": description, "error": err.Error()}).Infoln("error occurred ")
	} else {
		logrus.WithFields(logrus.Fields{"method": method, "description": description}).Infoln("error occurred ")
	}

	ctx.JSON(httpCode,
		errorResponse{
			Status: models.Status{
				StatusCode:        httpCode,
				StatusDescription: http.StatusText(httpCode),
				Description:       description,
			},
		})
}
