package routes

import "github.com/gin-gonic/gin"

// APIRoutes is a gin group
func APIRoutes(router *gin.RouterGroup) {
	router.GET("/api", getAPIResponse)
	return
}
