package routes

import (
	"errors"
	"html"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"launchpad.net/rjson"
	models "moolekkari.net/joice/zorodha/model"
)

func getAPIResponse(ctx *gin.Context) {
	period := html.EscapeString(ctx.Query("period"))
	symbol := html.EscapeString(ctx.Query("symbol"))
	if period == "" || symbol == "" {
		errorResponder(errors.New("required params not given"), "getAPIResponse", "unable to fetch",
			http.StatusInternalServerError, ctx)
		return
	}
	resp, err := getNSEData(period, symbol)
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err, "method": "getAPIResponse"}).
			Error()
		errorResponder(err, "getAPIResponse", "unable to fetch",
			http.StatusInternalServerError, ctx)
		return
	}
	type APIResponse struct {
		Status models.Status
		Result *models.NSEResponse
	}
	ctx.JSON(http.StatusOK, APIResponse{
		Status: models.Status{
			StatusCode:        http.StatusOK,
			StatusDescription: http.StatusText(http.StatusOK),
			Description:       "nse data",
		},
		Result: resp,
	})

	return
}

func getNSEData(period, symbol string) (*models.NSEResponse, error) {
	nseData := new(models.NSEResponse)
	base := "https://www.nseindia.com/corporates/corpInfo/equities/getBoardMeetings.jsp"

	baseURL, err := url.Parse(base)
	if err != nil {
		logrus.WithFields(logrus.Fields{"method": "getNSEData",
			"error": err}).
			Error("url error")
		return nseData, err
	}
	params := url.Values{}
	params.Add("period", period)
	params.Add("symbol", symbol)

	baseURL.RawQuery = params.Encode()

	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}

	resp, err := client.Get(baseURL.String())
	if err != nil {
		logrus.WithFields(logrus.Fields{"method": "getNSEData",
			"error": err, "request-url": baseURL.String()}).
			Error("request error")
		return nseData, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		logrus.WithFields(logrus.Fields{"method": "getNSEData",
			"error": "internal error", "statusCode": resp.StatusCode,
			"url": baseURL.String()}).
			Error("status error")
		return nseData, errors.New("unable to complete request")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.WithFields(logrus.Fields{"method": "getNSEData",
			"error": err}).
			Error("read error")
		return nseData, err
	}

	err = rjson.Unmarshal(body, nseData)
	if err != nil {
		logrus.WithFields(logrus.Fields{"method": "getNSEData",
			"error": err}).
			Error("unmarshal error")
		return nseData, err
	}
	return nseData, nil
}
